#!/bin/bash

tar zxvf curl-8.2.1.tar.gz

pushd curl-8.2.1 

./configure --without-ssl
make

popd
