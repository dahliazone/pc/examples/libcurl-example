# libcurl example

Simple example of how to build & link libcurl.

## Building

```
./unpack.sh
./build.sh
```

## Running

```
./curl-test
```

